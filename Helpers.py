import bcrypt
from fastapi import HTTPException


def encryptText(value: str) -> str:
    return bcrypt.hashpw(value.encode('utf-8'), bcrypt.gensalt()).decode('utf-8')


def verifyText(plainText: str, hashedText: str) -> bool:
    return bcrypt.checkpw(plainText.encode('utf-8'), hashedText.encode('utf-8'))


def getStringParts(separator, string: str) -> list:
    return string.split(separator)


def getTokenFromHeader(authorization) -> str:
    if not authorization or authorization is None or authorization == '':
        raise HTTPException(401, 'Unauthorized', {'WWW-Authenticate': 'Bearer'})

    return getStringParts(" ", authorization)[1]
