from Helpers import getTokenFromHeader
from fastapi import APIRouter, Request
from fastapi.responses import JSONResponse
from ManagerUser.Infrastructure.Controllers.BaseController import BaseController
from ManagerUser.Infrastructure.Requests.UserRequest import UserCreateRequest, UserUpdateRequest
from ManagerUser.Application.Mappers.UserDtoMapper import UserNewDtoMapper, UserUpdateDtoMapper
from ManagerUser.Application.Services.UserService import UserService


class UserController(BaseController):

    def __init__(self):
        super().__init__()
        self._userService = UserService()

    def getAll(self) -> JSONResponse:
        return JSONResponse({
            'code': 200,
            'success': True,
            'message': 'User list',
            'users': self._userService.setUser(self._user).getAll()
        }, 200)

    def getActiveAll(self) -> JSONResponse:
        return JSONResponse({
            'code': 200,
            'success': True,
            'message': 'User list actives',
            'users': self._userService.setUser(self._user).getActiveAll()
        }, 200)

    def show(self, id: int) -> JSONResponse:
        return JSONResponse({
            'code': 200,
            'success': True,
            'message': 'User found',
            'user': self._userService.setUser(self._user).findById(id)
        }, 200)

    def search(self, request: Request) -> JSONResponse:
        return JSONResponse({
            'code': 200,
            'success': True,
            'message': 'User list',
            'users': self._userService.setUser(self._user).search(request.query_params.get('search', ""))
        }, 200)

    def store(self, request: UserCreateRequest) -> JSONResponse:
        try:
            dto = UserNewDtoMapper().createFromRequest(request)
            user = self._userService.setUser(self._user).store(dto)
            return JSONResponse({
                'code': 200,
                'success': True,
                'message': 'Created user',
                'user': user
            }, 200)
        except Exception as ex:
            return JSONResponse({
                'code': 500,
                'success': False,
                'message': 'Error de servidor',
            }, 500)

    def update(self, id: int, request: UserUpdateRequest) -> JSONResponse:
        try:
            dto = UserUpdateDtoMapper().updateFromRequest(id, request)
            self._userService.setUser(self._user).update(dto)
            return JSONResponse({
                'code': 200,
                'success': True,
                'message': 'Updated user',
            }, 200)
        except Exception as ex:
            return JSONResponse({
                'code': 500,
                'success': False,
                'message': 'Error de servidor',
            }, 500)

    def delete(self, id: int):
        try:
            self._userService.setUser(self._user).delete(id)
            return JSONResponse({
                'code': 200,
                'success': True,
                'message': 'Deleted user',
            }, 200)
        except Exception as ex:
            return JSONResponse({
                'code': 500,
                'success': False,
                'message': 'Error de servidor',
            }, 500)


userRouter = APIRouter()
userController = UserController()


@userRouter.get('/get-all')
async def getAll(request: Request):
    userController.setUserFromToken(getTokenFromHeader(request.headers.get('authorization')))
    return userController.getAll()


@userRouter.get('/get-active')
async def getActive(request: Request):
    userController.setUserFromToken(getTokenFromHeader(request.headers.get('authorization')))
    return userController.getActiveAll()


@userRouter.get('/search')
async def search(request: Request):
    userController.setUserFromToken(getTokenFromHeader(request.headers.get('authorization')))
    return userController.search(request)


@userRouter.get('/{id}')
async def show(id: int, request: Request):
    userController.setUserFromToken(getTokenFromHeader(request.headers.get('authorization')))
    return userController.show(id)


@userRouter.post('/store')
async def store(userRequest: UserCreateRequest, request: Request):
    userController.setUserFromToken(getTokenFromHeader(request.headers.get('authorization')))
    return userController.store(userRequest)


@userRouter.post('/update/{id}')
async def update(id: int, userRequest: UserUpdateRequest, request: Request):
    userController.setUserFromToken(getTokenFromHeader(request.headers.get('authorization')))
    return userController.update(id, userRequest)


@userRouter.post('/delete/{id}')
async def delete(id: int, request: Request):
    userController.setUserFromToken(getTokenFromHeader(request.headers.get('authorization')))
    return userController.delete(id)
