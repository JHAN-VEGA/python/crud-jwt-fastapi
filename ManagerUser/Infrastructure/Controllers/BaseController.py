from ManagerAuth.Application.Services.ManagerAuthService import ManagerAuthService


class BaseController:
    def __init__(self):
        self._user = None
        self._authService = ManagerAuthService()

    def setUserFromToken(self, token: str):
        self._user = self._authService.getUserFromToken(token)

