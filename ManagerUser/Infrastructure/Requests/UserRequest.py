from pydantic import BaseModel, constr


class UserCreateRequest(BaseModel):
    name: constr(min_length=1, max_length=100)
    email: str
    password: constr(min_length=8)
    document_number: constr(min_length=1, max_length=20)
    active: bool = True


class UserUpdateRequest(BaseModel):
    name: constr(min_length=1, max_length=100)
    email: str
    document_number: constr(min_length=1, max_length=20)
    active: bool


class UserChangePasswordRequest(BaseModel):
    old_password: str
    new_password: constr(min_length=8)
    confirm_password: constr(min_length=8)
