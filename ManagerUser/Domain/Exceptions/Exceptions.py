from fastapi import HTTPException


def unauthorizedException():
    raise HTTPException(401, 'Unauthorized', {'WWW-Authenticate': 'Bearer'})
