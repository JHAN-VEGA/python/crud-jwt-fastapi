from ManagerUser.Domain.Dto.BaseDto import BaseDto


class UserNewDto(BaseDto):
    id: int
    name: str
    email: str
    password: str
    documentNumber: str
    active: bool = True


class UserUpdateDto(BaseDto):
    id: int
    name: str
    email: str
    documentNumber: str
    active: bool = True