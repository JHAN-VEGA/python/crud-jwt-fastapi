from abc import ABC, abstractmethod
from ManagerUser.Domain.Dto.BaseDto import BaseDto


class BaseMapper(ABC):

    @abstractmethod
    def getNewDto(self) -> BaseDto:
        pass
