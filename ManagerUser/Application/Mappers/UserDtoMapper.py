from ManagerUser.Application.Mappers.BaseMapper import BaseMapper
from ManagerUser.Infrastructure.Requests.UserRequest import UserCreateRequest, UserUpdateRequest
from ManagerUser.Domain.Dto.UserDto import UserNewDto, UserUpdateDto


class UserNewDtoMapper(BaseMapper):

    def getNewDto(self) -> UserNewDto:
        return UserNewDto()

    def createFromRequest(self, request: UserCreateRequest) -> UserNewDto:
        dto = self.getNewDto()
        dto.name = request.name
        dto.email = request.email
        dto.password = request.password
        dto.documentNumber = request.document_number
        dto.active = request.active
        return dto


class UserUpdateDtoMapper(BaseMapper):

    def getNewDto(self) -> UserUpdateDto:
        return UserUpdateDto()

    def updateFromRequest(self, id: int, request: UserUpdateRequest) -> UserUpdateDto:
        dto = self.getNewDto()
        dto.id = id
        dto.name = request.name
        dto.email = request.email
        dto.documentNumber = request.document_number
        dto.active = request.active
        return dto
