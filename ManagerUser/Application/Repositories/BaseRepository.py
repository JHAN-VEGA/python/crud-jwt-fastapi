from pony.orm import db_session
from abc import ABC, abstractmethod


class BaseRepository(ABC):

    def __init__(self, entity):
        self._query = None
        self._entity = entity
        self._user = None

    @abstractmethod
    def getNewEntity(self):
        pass

    def getEntity(self):
        return self._entity

    def setNewEntity(self):
        self._entity = self.getNewEntity()
        self.setNewQuery()
        return self

    def setEntity(self, entity):
        self._entity = entity
        return self

    @db_session
    def getAll(self) -> list:
        return self._entity.select()[:]

    @db_session
    def getActiveAll(self) -> list:
        return self._entity.select(lambda e: e.active == True)[:]

    @db_session
    def findById(self, id: int) -> dict:
        return self._entity.get(lambda e: e.id == id)

    @db_session
    def setNewQuery(self):
        self._query = None
        self._query = self._entity.select(lambda e: e)
        return self

    @db_session
    def where(self, condition):
        self._query = self._query.filter(condition)
        return self

    @db_session
    def get(self) -> list:
        return self._query[:]

    @db_session
    def delete(self, id: int):
        self.findById(id).delete()
        return self

    def setUser(self, user: dict):
        self._user = user
        return self
