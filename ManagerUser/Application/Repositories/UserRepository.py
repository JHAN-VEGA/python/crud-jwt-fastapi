from ManagerUser.Domain.Dto.UserDto import UserNewDto, UserUpdateDto
from ManagerUser.Application.Repositories.BaseRepository import BaseRepository
from Entities import User
from Helpers import encryptText
from pony.orm import db_session


class UserRepository(BaseRepository):

    def __init__(self):
        super().__init__(User)

    def getNewEntity(self):
        return User

    @db_session
    def store(self, dto: UserNewDto) -> User:
        user = self._entity(
            name = dto.name,
            email = dto.email,
            password = encryptText(dto.password),
            document_number = dto.documentNumber,
            active = dto.active
        )
        return user

    @db_session
    def update(self, dto: UserUpdateDto) -> User:
        user = self.findById(dto.id).set(
            name = dto.name,
            email = dto.email,
            document_number = dto.documentNumber,
            active = dto.active
        )
        return user

    def search(self, value: str) -> list:
        self.setNewEntity()
        self.where(lambda e: value in e.name or value in e.email or value in e.document_number)
        return self.get()
