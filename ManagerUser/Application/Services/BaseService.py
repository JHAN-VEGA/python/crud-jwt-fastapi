from abc import ABC
from ManagerUser.Domain.Exceptions.Exceptions import unauthorizedException


class BaseService(ABC):
    def __init__(self):
        self._user = None

    def setUser(self, user: dict):

        if not user:
            unauthorizedException()

        self._user = user
        return self
