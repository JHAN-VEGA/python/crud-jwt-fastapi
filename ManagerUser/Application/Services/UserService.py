from ManagerUser.Application.Services.BaseService import BaseService
from ManagerUser.Domain.Dto.UserDto import UserNewDto, UserUpdateDto
from ManagerUser.Application.Repositories.UserRepository import UserRepository
from fastapi import HTTPException


class UserService(BaseService):

    def __init__(self):
        super().__init__()
        self._userRepo = UserRepository()

    def getAll(self) -> list:
        users = self._userRepo.setUser(self._user).getAll()
        return [user.to_dict() for user in users]

    def getActiveAll(self) -> list:
        users = self._userRepo.setUser(self._user).getActiveAll()
        return [user.to_dict() for user in users]

    def findById(self, id: int) -> dict:
        user = self._userRepo.setUser(self._user).findById(id)
        if not user: raise HTTPException(404, 'User not found')
        return user.to_dict()

    def search(self, value: str) -> list:
        if not value: return []
        users = self._userRepo.setUser(self._user).search(value)
        return [user.to_dict() for user in users]

    def store(self, dto: UserNewDto):
        return self._userRepo.setUser(self._user).store(dto).to_dict()

    def update(self, dto: UserUpdateDto) -> object:
        self._userRepo.setUser(self._user).update(dto)
        return self

    def delete(self, id: int):
        self._userRepo.setUser(self._user).delete(id)
        return self
