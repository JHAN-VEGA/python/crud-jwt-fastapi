from dotenv import load_dotenv
from pathlib import Path
import os
from pony.orm import Database

env_path = Path('.') / '.env'
load_dotenv(dotenv_path=env_path)

db = Database()


class DatabaseConnection:

        def __init__(self):
        self.__connection = os.getenv('DB_CONNECTION')
        self.__host: str = os.getenv('DB_HOST', 'localhost')
        self.__port: str = os.getenv('DB_PORT', 5432)
        self.__database: str = os.getenv('DB_DATABASE')
        self.__username: str = os.getenv('DB_USERNAME')
        self.__password = os.getenv('DB_PASSWORD')

    def initialize_db(self) -> None:
        db.bind(
            provider=self.__connection,
            host=self.__host,
            port=self.__port,
            database=self.__database,
            user=self.__username,
            password=self.__password
        )
        db.generate_mapping()


class JWTSettings:

    def __init__(self):
        self.__algorithm = os.getenv('JWT_ALGORITHM')
        self.__expire_minutes = os.getenv('JWT_EXPIRE_MINUTES')
        self.__secret_key = os.getenv('JWT_SECRET_KEY')

    @property
    def secret_key(self) -> str:
        return str(self.__secret_key)

    @property
    def algorithm(self) -> str:
        return str(self.__algorithm)

    @property
    def expire_minutes(self) -> int:
        return int(self.__expire_minutes)
