from pony.orm import Required, PrimaryKey, Optional
from Settings import db


class User(db.Entity):
    _table_ = 'users'
    id = PrimaryKey(int, auto=True)
    name = Required(str)
    email = Required(str, unique=True)
    password = Required(str)
    document_number = Required(str, unique=True)
    active = Optional(bool, default=True)

    def to_dict(self) -> dict:
        return {
            'id': self.id,
            'name': self.name,
            'email': self.email,
            'document_number': self.document_number,
            'active': self.active,
        }
