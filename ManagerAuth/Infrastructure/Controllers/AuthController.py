from fastapi import APIRouter
from fastapi.responses import JSONResponse
from ManagerAuth.Infrastructure.Controllers.BaseController import BaseController
from ManagerAuth.Infrastructure.Requests.AuthRequest import LoginRequest
from ManagerAuth.Application.Services.ManagerAuthService import ManagerAuthService


class AuthController(BaseController):

    def __init__(self):
        super().__init__()
        self._managerAuthService = ManagerAuthService()

    def login(self, request: LoginRequest) -> JSONResponse:
        return JSONResponse({
            'code': 200,
            'success': True,
            'message': 'Generated token',
            'data': {
                'token': self._managerAuthService.login(request),
                'token_type': 'bearer'
            }
        }, 200)


authRouter = APIRouter()
authController = AuthController()


@authRouter.post('/login')
async def login(request: LoginRequest):
    return authController.login(request)
