from pydantic import BaseModel, constr


class LoginRequest(BaseModel):
    email: str
    password: constr(min_length=8)
