from jose import JWTError, jwt
from fastapi import HTTPException, status
from datetime import datetime, timedelta
from ManagerAuth.Infrastructure.Requests.AuthRequest import LoginRequest
from ManagerAuth.Application.Repositories.UserRepository import UserRepository
from Helpers import verifyText
from Settings import JWTSettings
from pony.orm import db_session


class ManagerAuthService:
    def __init__(self):
        self._userRepo = UserRepository()
        self.__token_data = {}
        self._jwt_settings = JWTSettings()

    def login(self, request: LoginRequest) -> str:
        user = self._userRepo.findByEmail(request.email)

        if not user or not user.active or not verifyText(request.password, user.password):
            raise HTTPException(
                status.HTTP_401_UNAUTHORIZED,
                'Invalid credentials',
                {
                    'WWW-Authenticate': 'Bearer'
                },
            )

        self.__token_data = {
            'sub': str(user.id),
            'name': user.name,
            'email': user.email,
            'active': user.active
        }

        return self.__generateToken()

    def __generateToken(self):
        self.__token_data.update({
            'exp': datetime.utcnow() + timedelta(minutes=self._jwt_settings.expire_minutes)
        })
        return jwt.encode(self.__token_data, self._jwt_settings.secret_key, self._jwt_settings.algorithm)


    @db_session
    def getUserFromToken(self, token: str) -> dict:

        unauthorizedException = HTTPException(401, 'Unauthorized', {'WWW-Authenticate': 'Bearer'})

        if token is None:
            raise unauthorizedException

        try:
            payload = jwt.decode(str(token), str(self._jwt_settings.secret_key), self._jwt_settings.algorithm)

            user_id = payload.get('sub')

            if user_id is None:
                raise unauthorizedException

        except JWTError as jwt_error:
            raise HTTPException(401, f'Invalid token: {jwt_error}')

        except ValueError as value_error:
            raise HTTPException(401, f'Error decoding token: {value_error}')

        return self._userRepo.findById(int(user_id)).to_dict()
