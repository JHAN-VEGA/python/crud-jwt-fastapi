from ManagerAuth.Application.Repositories.BaseRepository import BaseRepository
from Entities import User
from pony.orm import db_session


class UserRepository(BaseRepository):

    def __init__(self):
        super().__init__(User)

    @db_session
    def findByEmail(self, email: str) -> User:
        return self._entity.get(lambda e: e.email == email)
