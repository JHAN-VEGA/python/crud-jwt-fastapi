from abc import ABC


class BaseRepository(ABC):

    def __init__(self, entity):
        self._query = None
        self._entity = entity

    def findById(self, id: int) -> dict:
        return self._entity.get(lambda e: e.id == id)
