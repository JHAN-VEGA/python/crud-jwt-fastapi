from fastapi import FastAPI
from ManagerUser.Infrastructure.Controllers.UserController import userRouter
from ManagerAuth.Infrastructure.Controllers.AuthController import authRouter
from Settings import DatabaseConnection

connection = DatabaseConnection()

app = FastAPI(
    title='API DE PRUEBA',
    version='1.0.1',
    contact={
        'name': 'Jhan Sebastian Vega',
        'email': 'jhanvega01@outlook.com',
    },
)

connection.initialize_db()

app.include_router(router=authRouter, prefix='/api/v1/auth', tags=['auth'])
app.include_router(router=userRouter, prefix='/api/v1/users', tags=['users'])
